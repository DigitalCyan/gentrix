# gentrix
## What is this?
`gentrix` is a library for creating matrixes of generics. You can declare a matrix of any type. Handy functions included. 

## Examples
```rust
let mut matrix = Matrix::<i32>::new((5, 5));

assert!(matrix.set(1, (1, 1)).is_ok());
assert!(matrix.set(6, (1, 2)).is_ok());

let a = matrix.get((1, 1)).unwrap();
let b = matrix.get((1, 2)).unwrap();

assert_eq!(a + b, 7);
```

```rust
let items = vec![vec![1, 2], vec![3, 4]];

let mut matrix = Matrix::from(items);

matrix.translate((0, 0), (1, 0)).unwrap();

println!("{:?}", matrix);

assert_eq!(matrix.get((0,0)), None);
assert_eq!(matrix.get((1,0)), Some(1));
```

## Future plans
Refinement and proper documentation