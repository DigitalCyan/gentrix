use std::vec;

use crate::matrix::Matrix;

#[test]
fn basic() {
    let mut matrix = Matrix::<i32>::new((5, 5));

    assert!(matrix.set(1, (1, 1)).is_ok());
    assert!(matrix.set(6, (1, 2)).is_ok());

    let a = matrix.get((1, 1)).unwrap();
    let b = matrix.get((1, 2)).unwrap();

    assert_eq!(a + b, 7);
}

#[test]
fn from() {
    let items = vec![vec![1, 2], vec![3, 4]];

    let mut matrix = Matrix::from(items);

    assert_eq!(matrix.get((1, 0)), Some(3));
}

#[test]
fn translation() {
    let items = vec![vec![1, 2], vec![3, 4]];

    let mut matrix = Matrix::from(items);

    matrix.translate((0, 0), (1, 0)).unwrap();

    println!("{:?}", matrix);

    assert_eq!(matrix.get((0,0)), None);
    assert_eq!(matrix.get((1,0)), Some(1));
}

#[test]
fn out_of_bounds() {
    let mut matrix = Matrix::<bool>::new((5,5));

    assert!(matrix.set(true, (0,0)).is_ok());
    assert!(matrix.translate((0,0), (0,1)).is_ok());
    assert!(matrix.delete((5,5)).is_err());
}