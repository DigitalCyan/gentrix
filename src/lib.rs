mod matrix;

pub use crate::matrix::Matrix;

#[cfg(test)]
mod tests;
